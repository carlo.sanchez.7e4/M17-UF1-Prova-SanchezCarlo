using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class FirePoint : MonoBehaviour
{
    public GameObject firePointLeft;
    public GameObject firePointRight;
    public GameObject bullet;
    private int switcher;

    private void Start()
    {
        switcher = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {

            if (switcher == 0)
            {
                Fire(firePointLeft);
                Toggle();
            }
            else if (switcher == 1)
            {
                Fire(firePointRight);
                Toggle();
            }
        }
    }

    private void Toggle()
    {
        if (switcher == 0)
        {
            switcher++;
        } else if (switcher == 1)
        {
            switcher--;
        }
    }

    private void Fire(GameObject firePoint)
    {
        Instantiate(bullet, firePoint.transform.position, firePoint.transform.rotation);
    }
}