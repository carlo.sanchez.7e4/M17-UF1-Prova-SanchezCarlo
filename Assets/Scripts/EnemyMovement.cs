using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private float moveVertical;
    public float speed;

    public Rigidbody2D RB;
    public EnemyData ED;

    private GameObject player;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        movement();

        Vector3 stageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0));
        if (transform.position.y < stageDimensions.y)
        {
            Destroy(gameObject);
        }
    }

    private void movement()
    {

        /*switch (ED.selector)
        {
            case enemyType.small:
                moveVertical = -1 * speed;

                RB.velocity = new Vector2(0, moveVertical);
                break;
            case enemyType.big:
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed);
                break;
        }
        */
        moveVertical = -1 * speed;

        RB.velocity = new Vector2(0, moveVertical);

    }
}
