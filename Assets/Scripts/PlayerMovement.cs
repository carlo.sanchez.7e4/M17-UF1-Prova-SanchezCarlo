using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private float moveHorizontal;
    private float moveVertical;
    private float speed;

    public Rigidbody2D RB;
    private PlayerData PD;


    // Start is called before the first frame update
    void Start()
    {
        PD = gameObject.GetComponent<PlayerData>();
        speed = PD.speed; ;
    }

    // Update is called once per frame
    void Update()
    {
        movement();
    }

    private void movement()
    {
        moveHorizontal = Input.GetAxis("Horizontal") * speed;
        moveVertical = Input.GetAxis("Vertical") * speed;

        RB.velocity = new Vector2(moveHorizontal, moveVertical);
    }

}
