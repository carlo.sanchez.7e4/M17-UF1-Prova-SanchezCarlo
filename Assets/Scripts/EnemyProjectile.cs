using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public float speed = 10f;
    public Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = (-transform.up * speed);
    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.tag.Equals("Enemy"))
        {
            Debug.Log(collision.name);
            Destroy(gameObject);
            if (collision.gameObject.tag.Equals("Player"))
            {
                Destroy(collision.gameObject);
            }
        }
    }
}
