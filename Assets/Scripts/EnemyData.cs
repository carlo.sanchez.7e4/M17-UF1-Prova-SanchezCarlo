using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enemyType
{
    small,
    big
}

public class EnemyData : MonoBehaviour
{
    public enemyType selector;
    private int hp;
    private int value;

    public void TakeDamage(int damage)
    {
        hp -= damage;
    }

    private void Start()
    {
        switch (selector)
        {
            case enemyType.small:
                hp = 1;
                value = 5;
                break;
            case enemyType.big:
                hp = 5;
                value = 20;
                break;
        }
    }

    private void Update()
    {
        if (hp <= 0)
        {
            Destroy(gameObject);

            GameManager.Instance.UpdateScore(value);
        }
    }
}
