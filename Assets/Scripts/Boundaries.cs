using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Collider2D enemyCollider = collision.gameObject.GetComponent<Collider2D>();
            Collider2D collider = gameObject.GetComponent<Collider2D>();
            Physics2D.IgnoreCollision(enemyCollider, collider);
        }
    }
}
