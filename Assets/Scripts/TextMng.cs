using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Type
{
    score
}

public class TextMng : MonoBehaviour
{
    public Type selector;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (selector)
        {
            case Type.score:
                gameObject.GetComponent<Text>().text = "Score:\t" + GameManager.Instance.GetScore();
                break;
        }
    }
}
